﻿namespace ZajęciaStruktury
{
    public struct CrashInfo
    {
        public int DeadCount;
        public float ImpactVelocity;
        public Danger DangerDegree;

        public CrashInfo(int peopleCount, float car1Speed, float car2Speed)
        {
            DangerDegree = (peopleCount <= 2) ? Danger.Normal : Danger.Dangerous;
            DeadCount = peopleCount - 2;
            ImpactVelocity = car1Speed + car2Speed;
        }

        public override string ToString()
        {
            return DeadCount.ToString() + ", "
                + ImpactVelocity.ToString() + ", "
                + DangerDegree.ToString();
        }
    }

    class CrashHandler
    {
        public bool DidCarsCrash(Car car1, Car car2, out CrashInfo info)
        {
            info = new CrashInfo();
            if (car1.Position != car2.Position)
                return false;

            info = new CrashInfo(car1.Passengers + car2.Passengers,
                car1.Velocity, 
                car2.Velocity);

            car1.Velocity = 0f;
            car2.Velocity = 0f;

            return true;
        }
    }
}
