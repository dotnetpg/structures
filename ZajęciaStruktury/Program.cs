﻿using System;

namespace ZajęciaStruktury
{
    public struct Container
    {
        public int a, b;
        public Container(int A, int B)
        {
            a = A;
            b = B;
        }
    }

    public enum Działanie
    {
        Dodawanie,
        Odejmowanie,
        Mnożenie,
        Dzielenie
    }

    class Program
    {
        static Container GetContainer(int x, int y)
        {
            return new Container(x * x, x * y);
        }

        static void CalculateInts(ref int x, ref int y)
        {
            y = x * y;
            x = x * x;
        }

        static void AssignInts(out int x, out int y)
        {
            y = 2;
            x = 4;
        }

        static void DoEnumCalculator()
        {
            int a = 5, b = 11, wynik = 0;
            Działanie wybor = (Działanie)int.Parse(Console.ReadLine());
            switch (wybor)
            {
                case Działanie.Dodawanie:
                    wynik = a + b;
                    break;
                case Działanie.Odejmowanie:
                    wynik = a - b;
                    break;
                case Działanie.Mnożenie:
                    wynik = a * b;
                    break;
                case Działanie.Dzielenie:
                    wynik = a / b;
                    break;
            }
            Console.WriteLine(wynik.ToString());
            Console.ReadLine();
        }

        static void DoRefOutOperation()
        {
            int a, b;
            AssignInts(out a, out b);
            Console.WriteLine("{0}, {1}", a, b);
            CalculateInts(ref a, ref b);
            Console.WriteLine("{0}, {1}", a, b);

            Container container;
            container = GetContainer(2, 4);
            a = container.a;
            b = container.b;
            Console.ReadLine();
        }

        private static void DoCarsProject()
        {
            Car car1 = new Car(2, 65.5f, 4, CarMark.Mercedes);
            Car car2 = new Car(2, 30.0f, 2, CarMark.Volvo);
            Console.WriteLine(car1.ToString());
            Console.WriteLine(car2.ToString());
            Console.WriteLine();

            CrashHandler handler = new CrashHandler();
            CrashInfo info;
            if (handler.DidCarsCrash(car1, car2, out info))
            {
                Console.WriteLine("Auta się zderzyły");
                Console.WriteLine(info.ToString());
            }

            Console.WriteLine(car1.ToString());
            Console.WriteLine(car2.ToString());
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            //Uncomment these to see the operation executions:
            //DoEnumCalculator();
            //DoRefOutOperation();

            //The final project of this lesson:
            DoCarsProject();
        }
    }
}
