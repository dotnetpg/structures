﻿namespace ZajęciaStruktury
{
    public enum CarMark
    {
        Mercedes,
        Sedan,
        Volvo,
        Passat
    }

    public enum Danger
    {
        Normal,
        Dangerous
    }

    class Car
    {
        public int Position { get; private set; }
        public float Velocity { get; set; }
        public int Passengers { get; private set; }
        public CarMark Mark { get; private set; }

        public Car(int pos, float velo, int pass, CarMark mark)
        {
            Position = pos;
            Velocity = velo;
            Passengers = pass;
            Mark = mark;
        }

        public override string ToString()
        {
            return "Car " + Mark.ToString() + ", Velocity: " + Velocity.ToString() + ", Pos: " + Position.ToString();
        }
    }
}
